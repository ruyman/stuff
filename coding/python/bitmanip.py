# Let n and m be 32 bit integers
# Set bits i to j from n to those from n

def update_bits(n, m, i, j):
   """ Puts the i to j bits of m on the i to j positions of n """
   mask = ~0  # all one
   mask &= ~((1<<(i))-1)   # lower
   mask &=  ((1<<(j+1))-1) # upper
   n |= (mask & m)
   return n

def main(a, b):
   print " Set bit 3 , output should be 8"
   val = 0;
   val |= (1 << 3)
   print val
   print " Unset bit 3, output should be 0"
   mask = (1<<3)
   val &= ~(mask)
   print val
   print update_bits.__doc__
   print bin(int(a))
   print bin(int(b))
   val = update_bits(int(a), int(b), 1, 3)
   print bin(val)
   print "This should be 2: %d"%(high_bit(2))

def high_bit(n):
   """ Returns the highest bit number """
   num = n
   count = 0
   while num>0:
      count += 1
      num >>= 1
   return count

import sys

if __name__ == '__main__':
      if len(sys.argv) != 3:
         print " Incorrect number of options "
         print " Expected: ./%s numA numB "%(sys.argv[0])
         sys.exit(1)
      main(sys.argv[1], sys.argv[2])
   


