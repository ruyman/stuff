/* Using hwloc from C++
 */
#include <algorithm>
#include <iostream>
#include <numeric>

#include <hwlocxx>

int main(void)
{
   int depth;
   unsigned i, n;
   unsigned long size;
   int levels;
   int topodepth;

   hwlocxx::topology topo;

   /* Optionally, get some additional topology information
      in case we need the topology depth later. */
   topodepth = topo.get_depth();

   /*****************************************************************
    * First example:
    * Walk the topology with an array style, from level 0 (always
    * the system level) to the lowest level (always the proc level).
    *****************************************************************/
   for (depth = 0; depth < topodepth; depth++) {
      std::cout << "*** Objects at level " << depth << std::endl;
      for (i = 0; i < topo.get_width_at_depth(depth); i++) {
         std::cout << "Index " << i << ":" << topo.get_obj(depth, i)
                   << std::endl;
      }
   }

   /*****************************************************************
    * Sixth example:
    * Allocate some memory on the last NUMA node, bind some existing
    * memory to the last NUMA node.
    *****************************************************************/
   /* Get last node. There's always at least one. */
   n = topo.get_width_by_type(HWLOC_OBJ_NUMANODE);

   auto obj = topo.get_object_by_type(HWLOC_OBJ_NUMANODE, n - 1);

   hwlocxx::allocator<int> a{topo, obj};

   size_t nElems = 10u;
   std::vector<int, hwlocxx::allocator<int>> v1{nElems, a};
   std::iota(std::begin(v1), std::end(v1), 1);
   auto sum = std::accumulate(std::begin(v1), std::end(v1), 0, std::plus<>());
   auto sumResult = (nElems * (nElems + 1) / 2);
   return sumResult - sum;
}
