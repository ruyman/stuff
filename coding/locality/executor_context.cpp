/* Executor Context playground
 */
#include <algorithm>
#include <iostream>
#include <numeric>
#include <type_traits>
#include <future>

#include <hwlocxx>

// Missing from gcc 5
template< class T >
using result_of_t = typename std::result_of<T>::type; 


int main(void)
{
   // hwloc-based EC
   hwlocxx::experimental::ExecutionContext hwEC;

   auto mE = hwEC.executor();

   auto fut = mE.twoway_execute([&]() -> unsigned {
       std::cout << " Hello World " << std::endl;
       return 42u;
       });
    
   return fut.get();
};
