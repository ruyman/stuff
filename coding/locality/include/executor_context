
#ifndef EXECUTOR_CONTEXT

namespace experimental
{

class ExecutionContext
{
   public:
   ~ExecutionContext();

   ExecutionContext(ExecutionContext const&) = delete;
   ExecutionContext(ExecutionContext&&) = delete;

   execution_resource_t const& execution_resource() const noexcept;

   void executor() = delete;

   // Waiting functions:
   void wait() = delete;
   template <class Clock, class Duration>
   bool wait_until(chrono::time_point<Clock, Duration> const&) = delete;
   template <class Rep, class Period>
   bool wait_for(chrono::duration<Rep, Period> const&) = delete;

   private:
};
};

#endif // EXECUTOR_CONTEXT

// vim: filetype=cpp
