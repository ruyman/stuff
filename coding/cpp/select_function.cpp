#include <iostream>

#include <type_traits>

struct interop_handle {

};

struct has_handle {};
struct no_handle {};

template <int>
struct host_task_type;

template <>
struct host_task_type<0> {};

template <>
struct host_task_type<1> {};

class handler {
 public:
 template<typename T>
  auto get_host_task_type(T t)
      -> decltype(t(std::declval<interop_handle>()), has_handle{}) {
    return {};
  }

  template<typename T>
    auto get_host_task_type(T t) 
        -> decltype(t(), no_handle{}) {
    return {};
  }

  template <typename FunctorT>
  void call_host_task(FunctorT f, has_handle) {
    interop_handle ih;
    f(ih);
  }

  template <typename FunctorT>
  void call_host_task(FunctorT f, no_handle) {
    f();
  }

  template <typename FunctorT>
  void host_task(FunctorT func) {
    auto type = get_host_task_type(func);
    call_host_task(func, type);
  }
};

int foo() {
  handler h;
  h.host_task([=](){ printf("Hello World \n"); });
  h.host_task([=](interop_handle ih) {});
  return 0;
}

