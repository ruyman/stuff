#include <iostream>
#include <memory>

class interface;

// This is the implementation, avail. only inside the runtime
class interface_impl
{
               private:

        int _member;

        public:

        interface_impl(): _member(0) { };

        int get_member() { return _member; }
};

// This function enables other classes, not friends, to access 
// the private implementation object of those classes allowing it 
// (via a single friend declaration)
template<class Public, class Private>
Private& implementation_of(Public& p)
{
        return *(p.impl.get());
}

// This is the onyl class the user sees

class interface
{
        template<class Public, class Private>
        friend Private& implementation_of(Public& p);

        private:

                std::unique_ptr<interface_impl> impl;
        public:
                interface() : impl{ new interface_impl() } { };
                ~interface() { };

                int add(int number);
};

int interface::add(int number)
{
   return (impl->get_member() + number);
}

int main() 
{
        // This could be the code of any class
        interface i;
        std::cout << "Out: " << i.add(i.add(3)) << std::endl;

        std::cout << "Member: " 
                << implementation_of<interface, interface_impl>(i).get_member()
                << std::endl;

        // This is not valid, impl is private:
        // * std::cout << " User: " << *(i.impl) << std::endl;
        return 0;
}
