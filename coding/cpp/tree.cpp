#include <iostream>
#include <exception>
#include <iomanip>
#include <time.h>
#include <stack>
#include <cstdlib>
#include <iterator>

class InvalidNode: public std::exception
{
  public:
   virtual const char * what() const throw ( )
   {
      return " Invalid node ";
   }

};

template<typename T>
class BinaryTree
{
   struct Node
   {
      T _value;
      struct Node * _l;
      struct Node * _r;
      struct Node * _parent;

      explicit Node(const T& val): _value(val), _l(NULL), _r(NULL), _parent(NULL) { }
               Node() :  _l(NULL), _r(NULL), _parent(NULL) { }
               ~Node() { _l = NULL; _r = NULL; _parent = NULL;  }
   } * _root;
   

   protected:
   
      struct Node * find_node(const T& val, BinaryTree::Node * _node) throw ( );

      void show_node(const BinaryTree<T>::Node& node, std::ostream& stream) const;

      struct Node * getRoot( ) const;
     
   public:

      BinaryTree(): _root(NULL) { }
      ~BinaryTree();
      
      void insert(const T& val) throw ( );
      void remove(const T& val) throw ( );
      void show(std::ostream& stream) const;

  class TreeIterator
  {
    private:
       std::stack<Node *> _st;
       Node * _p;
    public:
     
      explicit TreeIterator(struct Node * n);
      explicit TreeIterator(TreeIterator * t);
      struct Node * next();
      struct Node * getPtr();
      bool operator==(BinaryTree<T>::TreeIterator * it);
      bool operator!=(BinaryTree<T>::TreeIterator * it);
      struct Node * operator++();
      T operator*();

  };

	TreeIterator * begin();
	TreeIterator * end();

};

template<typename T>
typename BinaryTree<T>::TreeIterator * BinaryTree<T>::begin()
{
   struct Node * left = _root;

   /* Get the smaller element */
   while (! (left->_l != NULL))
   {
      left = left->_l;
   }   
   return new TreeIterator(left);
}


template<typename T>
typename BinaryTree<T>::TreeIterator * BinaryTree<T>::end()
{
   struct Node * right = _root;

   /* Get the largest element */
   while (!(right->_r != NULL))
   {
      right = right->_r;
   }   

   return new TreeIterator(right);
}



template<typename T>
T BinaryTree<T>::TreeIterator::operator*()
{
	return this->getPtr()->_value;
}

template<typename T>
typename BinaryTree<T>::Node * BinaryTree<T>::TreeIterator::operator++()
{
	return this->next();
}


template<typename T>
BinaryTree<T>::TreeIterator::TreeIterator(BinaryTree<T>:: Node * n)
{
  _p = n;
}

template<typename T>
BinaryTree<T>::TreeIterator::TreeIterator(BinaryTree<T>::TreeIterator * t)
{
  _p = t->getPtr();
}

template<typename T>
typename BinaryTree<T>::Node * BinaryTree<T>::TreeIterator::getPtr()
{
	return _p;
}

template<typename T>
bool BinaryTree<T>::TreeIterator::operator==(BinaryTree<T>::TreeIterator * it)
{
	if (it->getPtr() == this->getPtr())
	{
		return true;
	}
	return false;
}

template<typename T>
bool BinaryTree<T>::TreeIterator::operator!=(BinaryTree<T>::TreeIterator * it)
{
	return !(it == this);
}

template<typename T>
typename BinaryTree<T>::Node * BinaryTree<T>::TreeIterator::next()
{
	if (_p)
	{
		if (_p->_r)
		{
			_st.push(_p->_r);
		}
		if (_p->_l)
		{
			_st.push(_p);
			_st.push(_p->_l);
		}
      struct Node * ret = _st.top();
	   _p = ret;
	   _st.pop();
		return ret;
	}
	else
	{
	   return NULL;
	}
	
}

template<typename T>
struct BinaryTree<T>::Node * BinaryTree<T>::getRoot( ) const
{
   return _root;
}

template<typename T>
struct BinaryTree<T>::Node * BinaryTree<T>::find_node(const T& val, struct BinaryTree<T>::Node * _node)  throw ( )
{
   if (_node == NULL)
   {
      throw InvalidNode();
   }
   if (_node->_value  == val)
   {
      return _node;
   }
   else if (_node->_value > val)
   {  
      if (_node->_l)
      {
         return find_node(val, _node->_l);
      }
      else
      {
         return _node;
      }
   }
   else if (_node->_value < val)
   {
      if (_node->_r)
      {
         return find_node(val, _node->_r);
      }
      else
      {
         return _node;
      }
   }
}

template<typename T>
void BinaryTree<T>::insert(const T& val) throw ( )
{
   if (_root == NULL)
   {
      _root = new Node(val);
   }
   else
   {
      struct BinaryTree<T>::Node * tmp = find_node(val, _root);
      if (tmp->_value == val)
      {
         // Already there
         return;
      }
      else
      {
         struct BinaryTree<T>::Node * n = new struct BinaryTree<T>::Node(val);
         if (val < tmp->_value)
         {
            if (!tmp->_l)
            {
               tmp->_l = n;
               n->_parent = tmp;
            }
            else
            {
               throw InvalidNode();
            }
         }
         else
         {
            if (!tmp->_r)
            {
               tmp->_r = n;
               n->_parent = tmp;
            }
            else
            {
               throw InvalidNode();
            }
         }
      }
   }
}

template<typename T>
void BinaryTree<T>::remove(const T& val) throw ( )
{
   struct BinaryTree<T>::Node * tmp = find_node(val, _root);
   struct BinaryTree<T>::Node * descendant = (!tmp->_l)?tmp->_r:tmp->_l;
   struct BinaryTree<T>::Node * parent = tmp->_parent;

   {
      if (!tmp->_l && !tmp->_r)
      {
         if (parent)
         {
                 if (parent->_l == tmp)
                 {
                    parent->_l = NULL;
                 }
                 else
                 {
                    parent->_r == NULL;
                 }
         }
         delete tmp;
      }
      else if ( (!tmp->_l && tmp->_r) || (tmp->_l && !tmp->_r) )
      {  
         if (tmp == _root)
         {
            _root = tmp->_r;  
         }
         else
         {
            if (parent->_l == tmp)
            {
               parent->_l = descendant;
            }
            else
            {
               parent->_r = descendant;
            }
         }
         delete tmp;
      }
      else
      {
         struct BinaryTree<T>::Node * parent = tmp->_parent; 
         struct BinaryTree<T>::Node * rightmost = tmp->_l; 
         while (rightmost->_r != NULL)
         {
            rightmost = rightmost->_r;
         }
         rightmost->_r = tmp->_r;

         if (rightmost->_r)
         {
            rightmost->_r->_parent = rightmost;
         }

         if (tmp != _root)
         {
                 if (parent->_l == tmp)
                 {
                         parent->_l = tmp->_l;
                 }
                 else
                 {
                         parent->_r = tmp->_l;
                 }
         }
         else
         {
            _root = tmp->_l;
         }
	
         delete tmp;
      }
   }
}

template<typename T>
void BinaryTree<T>::show_node(const BinaryTree<T>::Node& node, std::ostream& stream) const
{
   static int level = -1;
   level++;
   stream << (node._value) << std::endl;
   stream << std::setfill(' ') << std::setw(level*3) ;
   stream << "+";
   stream << std::setfill('-') << std::setw(3) ;
   if (node._l)
   {
      show_node(*node._l, stream);
   }
   else
   {
      stream << "#" << std::endl;
   }
   stream << std::setfill(' ') << std::setw(level*3) ;
   stream << "+";
   stream << std::setfill('-') << std::setw(3) ;

   if (node._r)
   {
      show_node(*node._r, stream);
   }
   else
   {
      stream << "#" << std::endl;
   }
   level--;
}

template<typename T>
void BinaryTree<T>::show(std::ostream& stream) const
{  
   stream << "* ";
   if (_root)
   {
      show_node(*_root, stream);
   }
   else
   {
      stream << std::endl;
   }
}


void print(int val)
{
  std::cout << val << ", ";
}

/*template<typename T>
void BinaryTree<T>::in_order(void (*func) (T val), BinaryTree<T>::Node * node)
{
   if (node)
   {
      in_order(func,node->_l);
      func(node->_value);
      in_order(func,node->_r);
   }
}*/


template<typename T>
std::ostream& std::operator<<(std::ostream& stream, const BinaryTree<T> &bt)
{
   bt.show(stream);
   return stream;
}


int main(int argc, char * argv[])
{
   BinaryTree<int> * t = new BinaryTree<int>();
   srand(time(NULL));
   try
   {
    std::cout << (*t) << std::endl;
    for (int i = 0; i < 10; i++)
    {
      t->insert(rand()%100);
    }
   } 
   catch (...)
   {
         std::cout << "Invalid node found while inserting " << std::endl;
   }
   std::cout << (*t) << std::endl;

   BinaryTree<int>::TreeIterator it(t->begin());

   std::cout << " Value " << (*it) << std::endl;
 
   ++it;

   std::cout << " Value " << (*it) << std::endl;

   return 0;
}


