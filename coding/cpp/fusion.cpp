#include <iostream>
#include <algorithm>
#include <vector>

template<typename functionT, typename T>
std::vector<T> map(functionT f, const std::vector<T>& v);

template<typename functionT, typename T>
std::vector<T> map(functionT f, const std::vector<T>& v) {
  std::vector<T> c;
  c.resize(v.size());
  for (int i = 0; i < c.size(); i++) {
    c[i] = f(v[i]);
  }
  return c;
}

template<typename functionT, typename T>
T reduce(functionT f, const std::vector<T>& v) {
  T sum = 0;
  for (auto i : v) {
    sum = f(sum, i); 
  }
  return sum;
}

int main(int argc, char * argv[]) {
   std::cout << " Hello World ! " << std::endl;  
   std::vector<int> v({1, 1, 1, 1});

   std::for_each(v.begin(), v.end(), [] (int x) { std::cout << x << ","; } );
   std::cout << std::endl;

   std::cout << " map(x + 1) " << std::endl;

   std::vector<int> v2 = map([] (int x) -> int { return x + 1; }, v);

   std::for_each(v2.begin(), v2.end(), [] (int x) { std::cout << x << ","; } );
   std::cout << std::endl;
  
   std::cout << " reduce(+, map(x+1)) " << std::endl;

   int v3 = reduce([&] (int x, int y) -> int { return x + y; }, 
                              map([&] (int x) -> int { return x + 1; }, v2));

   std::cout << " v3 : " << v3 << std::endl;
   std::cout << std::endl;

   return 0;
}
