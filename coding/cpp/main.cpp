#include "mailbox.h"


PostalOffice po;


void main_menu_loop();
void show_mailboxes();
void edit_mailbox( );
void add_mailbox( );

int get_int_from_user( )  throw ( );


int main(int argc, char * argv[])
{
   int n_mb;

  
   cout << " Postal Office simulator! " << endl;

#if 0
   try
   {
      n_mb = get_mb_from_user();
   } 
   catch (TooManyMB& e)
   {
      cout << e.what() << endl; 
      /* Defaults to 10 Mailboxes */
      n_mb = 10;  
   }

   for (int i = 0; i < n_mb; i++)
   {
      po.push_back(new Mailbox(i));
   }
#endif

   main_menu_loop();
   
   return 0;
}


void main_menu_loop()
{
   const int NUM_OPTS = 4u;
   short int op;
   void (* menu_options[NUM_OPTS]) (void) = { 
                                              &show_mailboxes,
                                              &add_mailbox,
                                              &edit_mailbox
                                            };

   op = -1;
   while (op != 0)
   {
           cout << " Postal Office Simulation " << endl;
           cout << " ---------------------------- " << endl;
           cout << " 1 - Show all mailboxes " << endl; 
           cout << " 2 - Add mailbox " << endl;
           cout << " 0 - Exit " << endl;
           cout << " Enter your option " << endl;
           if (cin >> op)
           {
                  if (op > 0 && op < NUM_OPTS)
                  {
                     menu_options[op - 1]();
                  } 
                  else if (op != 0)
                  {
                               cout << " Incorrect option " << endl;  
                               op = -1;
                               break;
                  }
          } 
          else
          {
             cin.clear();
             cin.ignore(INT_MAX, '\n'); // ignore this line we couldn't read it
             op = -1;
          }
   }
}


void show_mailboxes( )
{
   PostalOffice::iterator mb;

   for (mb = po.begin(); mb != po.end(); mb++)
   {
      cout << *(*mb);
        
      cout << endl;
   }
}

void add_mailbox( )
{
   short unsigned int type;
   string names[2] = { "Private", "Company" };
   Mailbox * pm;

   type = 0;

   cout << " Create a new mailbox " << endl;
   cout << " Type of mailbox (1 private 2 company) " << endl;
   while ( type != 1 && type != 2)
   {
      try
      {
         type = get_int_from_user( );
      } 
      catch (exception)
      {
         cout << " Invalid input " << endl;
      }
      cout << " --- " << endl;
   }  
   cout << " Type is " << names[type - 1] << endl;
   if (type == 1)
   {
      pm = new PrivateMailbox(po.size());
      (static_cast<PrivateMailbox *>(pm))->setFirstName("Name");
      (static_cast<PrivateMailbox *>(pm))->setLastName("Last Name");
   }
   else if (type == 2)
   {
      pm = new CompanyMailbox(po.size());
      (static_cast<CompanyMailbox *>(pm))->setCompanyName("Company");
   }
   po.push_back(pm);
}

void edit_mailbox( )
{
   return;
}

int get_int_from_user( )  throw ( )
{
   int n_mb;

   if (cin >> n_mb)
   {
      return n_mb;
   }
   else
   {
      cin.clear();
      cin.ignore(INT_MAX, '\n'); // ignore this line we couldn't read it
      throw BadUserInput();
   }
   return n_mb;
}

