#ifndef MAILBOX_H
#define MAILBOX_H

#include <iostream>
#include <vector>
#include <exception>
#include <iomanip>

using namespace std;

const unsigned int MAX_MB = 10u;

class TooManyMB: public exception
{
  public:
   virtual const char * what() const throw ( )
   {
      return " Too Many MailBoxes ! ";
   }

};

class BadUserInput: public exception
{
  public:
   virtual const char * what() const throw ( )
   {
      return " Invalid User Input ";
   }

};

class Mailbox
{
   private:
      int _id;


   public:
      
      Mailbox(const int& id) : _id(id) { } ;
      ~Mailbox();

      void setBoxId(const int& id);
      const int& getBoxId() const;

      virtual void show_data(std::ostream& stream) const { };

};


std::ostream& operator<<(std::ostream& stream, const Mailbox& mb);

class CompanyMailbox: public Mailbox
{
   private:

      string companyName;

   public:
      CompanyMailbox(const int& id) : Mailbox(id), companyName("") { };
      ~CompanyMailbox();

      void setCompanyName(const string& name) ;
      const string& getCompanyName() const;

      void show_data(std::ostream& stream) const;
};

class PrivateMailbox: public Mailbox
{
   private:

      string firstName;
      string lastName;

   public:
      PrivateMailbox(const int& id) : Mailbox(id), firstName(""), lastName("") { };
      ~PrivateMailbox();

      void setFirstName(const string& name) ;
      const string& getFirstName() const;

      void setLastName(const string& name) ;
      const string& getLastName() const;

      void show_data(std::ostream& stream) const;

};

typedef vector<Mailbox *> PostalOffice;



#endif
