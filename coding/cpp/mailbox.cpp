#include "mailbox.h"


using namespace std;

void Mailbox::setBoxId(const int& id)
{
   this->_id = id;
}


const int& Mailbox::getBoxId() const
{
   return _id;
}


Mailbox::~Mailbox()
{
   // Nothing to do 
}

std::ostream& operator<<(std::ostream& stream, const Mailbox& mb)
{
   stream << " ID " << mb.getBoxId() << std::endl;
   mb.show_data(stream);
   return stream;
}


void PrivateMailbox::show_data(std::ostream& stream) const
{
   stream << setw(5) << getFirstName() << " " << getLastName() << " " << std::endl;
}


void CompanyMailbox::show_data(std::ostream& stream) const
{
   stream << setw(5) << getCompanyName() << std::endl;
}

void CompanyMailbox::setCompanyName(const string& name) 
{
   companyName = name;
}


const string & CompanyMailbox::getCompanyName() const
{
   return companyName;
}


void PrivateMailbox::setFirstName(const string& name) 
{
   firstName = name;
}


const string& PrivateMailbox::getFirstName() const
{
   return firstName;
}


void PrivateMailbox::setLastName(const string& name)
{
   lastName = name;
}


const string& PrivateMailbox::getLastName() const
{
   return lastName;
}

