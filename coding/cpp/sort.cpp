#include <iostream>

#include <vector>
#include <algorithm>

#define N 10

struct show_int_class
{
   void operator() (int i) 
   {
      std::cout << i << ", ";
   }
} show_int;



/* Template recursivity */

struct init
{

int elem;

template<int n>
void operator()(std::vector<int>& v)
{
   this->template operator()<n-1>(v);
   v.push_back(++elem);
}

} init_int;

template<>
void init::operator()<1>(std::vector<int>& v)
{

   elem = 0;
   v.push_back(elem);
}


template<typename T>
void qsort(std::vector<T>& v, int left, int right)
{
   if (left < right)
   {
      int pivot = ((right - left)>>1) + left;
      T store = left;
      
      std::swap(v[pivot], v[right]);

      for (int i = left; i < (right); i++)
      {
         if (v[i] < v[right])
         {
            std::swap(v[i],v[store]);
            store++;
         }
      }
      std::swap(v[store],v[right]);
      pivot = store;
      qsort<T>(v, left, pivot - 1);
      qsort<T>(v, pivot + 1, right);
   }
}

template <typename T>
void sort(std::vector<T>& v)
{
 qsort<T>(v, 0, v.size()-1);
}



int main(int argc, char * argv[])
{
   
// This is supported on C++11
//   std::vector<int> v1 = { 1, 2, 4 }; 

   std::vector<int> v1;
   std::vector<int>::iterator it;
   
   init_int.operator()<N>(v1);
   std::random_shuffle(v1.begin(), v1.end());
   std::for_each(v1.begin(), v1.end(), show_int);
   if ( (it = std::find(v1.begin(), v1.end(), 7)) == v1.end())
   {
      std::cout << " Not inside " << std::endl;
   }
   else
   {
      std::cout << " Element inside pos> " << std::distance(v1.begin(), it) << std::endl;
   }
   std::cout << std::endl;

   // std::sort(v1.begin(), v1.end());
   sort<int>(v1);

   std::for_each(v1.begin(), v1.end(), show_int);
   std::cout << std::endl;

   

   return 0;
}
