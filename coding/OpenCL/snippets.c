// 
                {
                  size_t val;
                  status = clGetDeviceInfo(_devices[0], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &val, NULL);
                  std::cout << ":: MAX WORK GROUP SIZE " << val << std::endl;
                  status = clGetDeviceInfo(_devices[0], CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t), &val, NULL);
                  std::cout << ":: MAX WORK ITEM SIZES " << val << std::endl;
                }
                

// Print devices              
  if (DEBUG_ENABLE)
                {
                        for (unsigned int i = 0; i < numPlatforms; i++)
                        {
                                char buf[100];
                                printf ("Platform %u: \n", i);
                                status = clGetPlatformInfo (platforms[i], CL_PLATFORM_VENDOR,
                                                sizeof (buf), buf, NULL);
                                printf ("\tVendor: %s\n", buf);
                                status |= clGetPlatformInfo (platforms[i], CL_PLATFORM_NAME,
                                                sizeof (buf), buf, NULL);
                                printf ("\tName: %s\n", buf);

                                if (status != CL_SUCCESS)
                                {
                                        printf ("clGetPlatformInfo failed\n");
                                        exit (255);
                                }
                        }
                        printf ("\n");
                }

// Print out some basic information about each device
                if ( DEBUG_ENABLE)
                {
                        printf ("%u devices detected\n", _numDevices);
                        for (unsigned int i = 0; i < _numDevices; i++)
                        {
                                char buf[100];
                                printf ("Device %u: \n", i);
                                status = clGetDeviceInfo (_devices[i], CL_DEVICE_VENDOR,
                                                sizeof (buf), buf, NULL);
                                printf ("\tDevice: %s\n", buf);
                                status |= clGetDeviceInfo (_devices[i], CL_DEVICE_NAME,
                                                sizeof (buf), buf, NULL);
                                printf ("\tName: %s\n", buf);
                                if (status != CL_SUCCESS)
                                {
                                        printf ("clGetDeviceInfo failed\n");
                                        exit (255);
                                }
                        }
                        printf ("\n");
                } else 
