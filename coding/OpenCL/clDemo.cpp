/***************************************
 *   clDemo.cpp
 *
 *   Author: Ruyman Reyes Castro <namiurtf@gmail.com>
 *
 *   Objective:
 *
 *    To create a program that loads an OpenCL C kernel from a file
 *    and executes it. Use as many C++11/C++14 features as possible.
 *
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <CL/cl.h>

#define CHECK_CL(cmd)                                                          \
  {                                                                            \
    cl_int _err = cmd;                                                         \
    if (_err != CL_SUCCESS) {                                                  \
      print_cl_error(_err);                                                    \
    }                                                                          \
  }

void print_cl_error(cl_int error) {
  std::cout << "The OpenCL command has failed with error " << error
            << std::endl;
}

template <typename rType, typename clObjectT> struct get_info_class {

  template <typename clFunctionT>
  static rType get_info(clFunctionT clF, clObjectT id, cl_int parameter) {}
};

template <typename clObjectT> struct get_info_class<std::string, clObjectT> {

  template <typename clFunctionT>
  static std::string get_info(clFunctionT clF, clObjectT id, cl_int parameter) {
    size_t strSize = 0;
    CHECK_CL(clF(id, parameter, 0, NULL, &strSize));
    char *platformName = (char *)malloc(sizeof(char) * strSize);
    CHECK_CL(clF(id, parameter, strSize, platformName, NULL))
    return std::string(platformName);
  }
};

class show_base {
protected:
  std::ostream &stream_;
  cl_uint parameter_;

public:
  show_base(std::ostream &stream) : stream_(stream) {};
};

template <cl_int nameT> class show_platform_info : show_base {
public:
  using show_base::show_base;

  // The default behaviour is to do nothing
  void operator()(cl_platform_id id) {}
};

#define GENERATE_SHOW_PLATFORM_INFO(PARAM_NAME, RTYPE)                         \
  template <> class show_platform_info<PARAM_NAME> : show_base {               \
  public:                                                                      \
    typedef RTYPE __rtype;                                                     \
                                                                               \
    using show_base::show_base;                                                \
                                                                               \
    void operator()(cl_platform_id id) {                                       \
      __rtype info_string = get_info_class<__rtype, cl_platform_id>::get_info( \
          clGetPlatformInfo, id, PARAM_NAME);                                  \
      stream_ << " " << #PARAM_NAME << " :" << info_string << std::endl;       \
    };                                                                         \
  };

GENERATE_SHOW_PLATFORM_INFO(CL_PLATFORM_NAME, std::string)
GENERATE_SHOW_PLATFORM_INFO(CL_PLATFORM_VENDOR, std::string)
GENERATE_SHOW_PLATFORM_INFO(CL_PLATFORM_PROFILE, std::string)
GENERATE_SHOW_PLATFORM_INFO(CL_PLATFORM_EXTENSIONS, std::string)
GENERATE_SHOW_PLATFORM_INFO(CL_PLATFORM_VERSION, std::string)

int main(int argc, char *argv[]) {
  std::cout << " Demonstrating OpenCL capabilities " << std::endl;
  std::cout << " The following OpenCL Platforms are available: " << std::endl;

  cl_int error;
  std::vector<cl_platform_id> platforms;
  cl_device_id device;
  cl_uint numPlatf, devices;

  // Fetch the Platform and Device IDs; we only want one.
  CHECK_CL(clGetPlatformIDs(0, NULL, &numPlatf));

  std::cout << " The system has " << numPlatf
            << " OpenCL platforms available " << std::endl;

  std::cout << std::string(20, '=') << std::endl;

  platforms.resize(numPlatf);

  CHECK_CL(clGetPlatformIDs(platforms.size(), platforms.data(), &numPlatf));

  std::for_each(platforms.begin(), platforms.end(),
              [&] (cl_platform_id pId) {
 
                show_platform_info<CL_PLATFORM_NAME> name(std::cout);
                show_platform_info<CL_PLATFORM_VENDOR> vendor(std::cout);
                show_platform_info<CL_PLATFORM_PROFILE> profile(std::cout);
                show_platform_info<CL_PLATFORM_EXTENSIONS> extensions(std::cout);
                show_platform_info<CL_PLATFORM_VERSION> version(std::cout);
                name(pId);
                vendor(pId);
                profile(pId);
                extensions(pId);
                version(pId);
                std::cout << std::string(20, '=') << std::endl;
              }
  );

  std::cout << " Everything ended up OK " << std::endl;
  return 0;
}
