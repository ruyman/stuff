#include <mpi.h>
#include <stdio.h>

#include "MpiChannel.hpp"

int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Print off a hello world message
    printf("Hello world from processor %s, rank %d"
           " out of %d processors\n",
           processor_name, world_rank, world_size);

    {
      size_t nElems = 100u;
      MPIChannel c(world_rank, world_size, nElems * sizeof(float));
#if 0
      // Initialization of memory, p 0 initialises all
      if (world_rank == 0) {
        auto  p = c.get<float>(0, nElems);
        for (size_t i = 0; i < nElems; i++) {
          p.get()[i] = 0.0f;
        }
      }
#endif // 0
    
      MPI_Barrier(MPI_COMM_WORLD);

#if 0
      // We get all the contents from p 1
      if (world_rank == 1) {
        auto  p = c.get<float>(0, nElems);
        for (size_t i = 0; i < nElems; i++) {
          if (p.get()[i] != 3.0) {
            std::cout << "PUT: " << i << " KO ( " << p.get()[i] << ")" << std::endl;
          }
        }
      }
#endif  // 0
    }   // End of channel scope
    // Finalize the MPI environment.
    MPI_Finalize();
    return 0;
}
