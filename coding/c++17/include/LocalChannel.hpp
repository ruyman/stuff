#include <memory>
#include <vector>
#include <mutex>
#include <cstring>
#include <cassert>

#define _ctplThreadPoolLength 500
#include "channels.hpp"

/** LocalChannel.
* A channel that communicates local threads.
* The channel stores an allocation of memory that is accessible
* by threads in the channel.
* This kind of channel is only used for testing, since threads
* can use other more efficient mechanism to share memory.
*/
template <>
class Channel<channels::Local>  {
 static std::mutex _m;
 std::shared_ptr<void> _ptr;

 public:

 Channel(size_t maxNumElems) : _ptr{ new char[maxNumElems], 
                      [&](char const * ptr) { delete [] ptr; } } 
  {};
 Channel(const Channel&) = delete;
 Channel(Channel&&) = delete;
 ~Channel() = default;

 // Put
 template<typename U>
 void put(off_t off, size_t nElems, U * ptr) {
  U * start = static_cast<U*>(_ptr.get());
  std::memcpy(&start[off], ptr, nElems * sizeof(U));
 }

 // Get
 template<typename U>
 locked_page<U> get(off_t off, size_t nElems);
};

template<typename U>
locked_page<U> Channel<channels::Local>::get(off_t offset,
                                             size_t nElems) {
 return locked_page<U>(&(static_cast<U*>(_ptr.get())[offset]), _m);
}

