#include <memory>
#include <vector>
#include <mutex>
#include <cstring>
#include <cassert>

#include "channels.hpp"

#include <mpi.h>

/** MPIChannel.
* A channel that communicates remote MPI Ranks.
*/
template <>
class Channel<channels::MPI>  {
 int _totalRanks;  // Total Number of Ranks
 int _myRank;   // My Rank
 void * _ptr;   // Pointer to the MPI window
 MPI_Win _mpiWin;

 public:

 Channel(int myRank, int totalRanks, size_t totalSize) :
          _totalRanks{totalRanks}, _myRank{myRank}, _mpiWin{} { 
    int status = MPI_Win_allocate(totalSize, sizeof(char), MPI_INFO_NULL,
                                  MPI_COMM_WORLD, &_ptr, &_mpiWin);
    if (status != MPI_SUCCESS) {
      std::cout << " Invalid object " << std::endl;
    }
 };

 Channel(const Channel&) = delete;
 Channel(Channel&&) = delete;
 ~Channel() {
   MPI_Win_free(&_mpiWin);
 }

 // Put
 template<typename U>
 void put(off_t off, size_t nElems, U * ptr) {
  // Figure out if off + nElems fits on my local space
  // Write the part that fits on my local space
  // Put the part that is remote
  U * start = static_cast<U*>(_ptr);
  std::memcpy(&start[off], ptr, nElems * sizeof(U));
 }

 // Get
 template<typename U>
 locked_page<U> get(off_t off, size_t nElems);
};

template<typename U>
locked_page<U> Channel<channels::MPI>::get(off_t offset,
                                             size_t nElems) {
 return _ptr; // locked_page<U>(&(static_cast<U*>(_ptr)[offset]));
}

