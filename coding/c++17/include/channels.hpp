#include <memory>
#include <atomic>
/**
 * Channel types
 */
enum class channels {
  Local,
  MPI
};

template <typename T, typename ChannelSync = std::mutex>
class locked_page {
  T * _ptr;
  struct control_block {
    std::atomic<int> _count;
    ChannelSync& _m;
    control_block(ChannelSync& m) : _m{m}, _count(0) { };

    void add_client() { 
      if (_count++ == 0) {
        _m.lock();
      }
    };
    void remove_client() { 
      if (--_count == 0) {
        _m.unlock();
      } 
    };
    int use_count() {
      return _count;
    }
    ~control_block() = default;
  };
  std::shared_ptr<control_block> _ctrlBlock;

  public:

  locked_page(T * ptr, ChannelSync& m) 
    : _ptr{ptr},  _ctrlBlock{std::make_shared<control_block>(m)} { 
      _ctrlBlock->add_client();
    }

  locked_page(const locked_page& rhs)
    : _ptr{rhs._ptr}, _ctrlBlock{rhs._ctrlBlock} {
      // No need to lock, all instances of this
      // object
      _ctrlBlock->add_client();
  };

  ~locked_page() {
    _ctrlBlock->remove_client();
  }

  T* get() {
    return _ptr;
  }
};

/** Channel.
 * Generic Channel interface
 */
template<channels ChannelT>
class Channel {

  public:
	  Channel() = default;
  	Channel(const Channel&) = default;
  	Channel(Channel&&) = default;
    ~Channel() = default;

    // Put
    template<typename U>
    void put(off_t off, size_t nElems, U * ptr) = delete;

    // Get
    template<typename T>
    locked_page<T> get(off_t offset, size_t nElems) = delete;
};

using LocalChannel = Channel<channels::Local>;
using MPIChannel = Channel<channels::MPI>;
