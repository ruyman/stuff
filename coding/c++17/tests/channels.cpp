#include <iostream>
#include <thread>

#include "LocalChannel.hpp"
#include "thread_pool.hpp"

// Channel<ThreadExecutor>
void get_example(std::thread::id pid, 
                 size_t start, size_t chunkSize, LocalChannel& c) {
	{ // For the duration of this block, no other thread can access the channel
		auto  p = c.get<float>(start * chunkSize, chunkSize);
#ifdef VERBOSE
    std::cout << " pid : " << pid << " start " 
              << start << " chunkSize " << chunkSize << std::endl;
#endif  // VERBOSE
    for (size_t i = 0; i < chunkSize; i++) {
      p.get()[i] = start * chunkSize + i;
    }
	}
}

void put_example(std::thread::id pid, size_t i, LocalChannel& c) {
  { // Every thread can put its value on the channel, no need for sync
    float val = 3.0;
    c.put<float>(i, 1, &val);
  }
}

int main()
{
	std::cout << " Channels Example " << std::endl;
  // Thread pool with two threads
  // third-party
  using nbsdx::concurrent::ThreadPool;
  ThreadPool<> tp;

  {
    LocalChannel c(500ul);

    size_t nElems = 100ul;
    // Initialization of memory
    {
      auto  p = c.get<float>(0, nElems);
      for (size_t i = 0; i < nElems; i++) {
        p.get()[i] = 0.0f;
      }
    }

    // We initialize each element of the memory
    // to its position,
    // but we do it in chunks of chunkSize.
    size_t chunkSize = 10u;
    size_t numChunks = nElems / chunkSize;
    for (size_t cId = 0; cId < numChunks; cId++) {
      tp.AddJob([=,&c]() {
          get_example(std::this_thread::get_id(), cId, chunkSize, c);
          });;
    }

    tp.WaitAll();
    // We ensure that each position of the array match the index
    {
      auto  p = c.get<float>(0, nElems);
      for (size_t i = 0; i < nElems; i++) {
        if (p.get()[i] != i) {
          std::cout << "GET: " << i << " KO ( " << p.get()[i] << ")" << std::endl;
        }
      }
    }

    // We put directly a 3 on the 3 position
    for (size_t i = 0; i < nElems; i++) {
      tp.AddJob([=,&c]() {
            put_example(std::this_thread::get_id(), i, c);
      });
    }

    // Put is asynchronous so we need to wait for all threads
    // to finish before the data is available
    tp.WaitAll();
    // We get all the contents, and ensure there is a 3 everywhere
    {
      auto  p = c.get<float>(0, nElems);
      for (size_t i = 0; i < nElems; i++) {
        if (p.get()[i] != 3.0) {
          std::cout << "PUT: " << i << " KO ( " << p.get()[i] << ")" << std::endl;
        }
      }
    }
  }
	return 0;
}
