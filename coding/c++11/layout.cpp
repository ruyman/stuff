#include <type_traits>
#include <iostream>

class base {
  static int varA;
};

class standard_layout : public base {
  int varA;
  int varB;

  public:

  standard_layout() {};

  int foo() { return 1; }
};

class trivial_constructor : public base {

    int varA;
  public:
    int varB;

    trivial_constructor() = default;
};


class pod : public base {
    int varA;
    int varB;

  public:

    pod() = default;
};

int main()
{
        int n = 3;
        std::cout << " Class standard_layout: " << std::endl;
        if (std::is_standard_layout<standard_layout>::value) {
          std::cout << " The class is standard layout " << std::endl;
        }
        if (std::is_trivial<standard_layout>::value) {
          std::cout << " The class is trivial " << std::endl;
        }
        std::cout << " Class trivial_constructor: " << std::endl;
        if (std::is_trivial<trivial_constructor>::value) {
          std::cout << " The class is trivial " << std::endl;
        }
        if (std::is_standard_layout<trivial_constructor>::value) {
          std::cout << " The class is standard_layout " << std::endl;
        }
        std::cout << " Class pod: " << std::endl;
        if (std::is_trivial<pod>::value) {
          std::cout << " The class is trivial " << std::endl;
        }
        if (std::is_standard_layout<pod>::value) {
          std::cout << " The class is standard_layout " << std::endl;
        }
        return 0;
}
