#include <iostream>
#include <memory>
#include <functional>


class CLWrap
{
        protected:

                const char * _fname;
                const char *_msg;

        public:

        CLWrap() : _fname(0), _msg(0) { };

        CLWrap(const char * fname, const char * msg):
                _fname(fname), _msg(msg) { };

        virtual ~CLWrap() { };

        virtual bool prefix() = 0;

        virtual void handle_error(int err)
        {
               std::cout << " Func " << _fname << " err : " << _msg << std::endl;
        }       

        void set_params(const char * fname, const char * msg)
        {
                _fname = fname;
                _msg = msg;
        }

        template<typename functorT>
        void run(functorT lambda)
        {
                int err;
                if (prefix())
                {
                        err = lambda();
                }
                if (err != 0)
                {
                        handle_error(err);
                }
        }


};


class RealCLWrap : public CLWrap
{
       

        public:
        RealCLWrap() : CLWrap() { };
        RealCLWrap(const char * fname,
                        const char * err_msg):
                CLWrap(fname, err_msg) { }

        virtual bool prefix() 
        {
                return 1;
        }

};


class FakeCLWrap : public CLWrap
{

        public:

        FakeCLWrap() : CLWrap() { };
        FakeCLWrap(const char * fname,
                        const char * err_msg):
                CLWrap(fname, err_msg) { }

        virtual bool prefix() 
        {
                return 0;
        }


};

CLWrap * __cw;

#ifndef __RELEASE__
#define CL_CALL(fname, params, msg) {\
                if (!__cw)\
                {\
                        fname params ;\
                }\
                else\
                {\
                __cw->set_params(#fname, msg);\
                __cw->run([&]()->int { return fname params; });\
                }\
        }
#else
#define CL_CALL(fname, params, msg) { fname params; }
#endif

int clFoo ( int * a, int * b );


int main()
{
        int n = 3;
        int data[3] = { 3, 4, 5};
        int out[3];

        // This calls the real
        CL_CALL ( clFoo, ( data, out ), "Real Call" );

        // This calls the fake one
        __cw = new FakeCLWrap();
        CL_CALL ( clFoo, ( data, out ), "Fake call" );
        delete __cw;

 //       std::cout << out[0] << "," << out[1] << "," << out[2] << std::endl;

        return 0;
}



int clFoo ( int * a, int * b )
{
        std::cout << " Inside clFoo " << std::endl;
        b[0] = a[0];
        b[1] = a[1];
        b[2] = a[2];
        return 27;
}
