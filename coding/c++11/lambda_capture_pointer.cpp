#include <iostream>

class foo
{
        private:
                int * member;

        public:

                foo(int * val): member(val) { };

        void operator()()
        {
                auto bar  = ([=]  { 
                                         (*member) = (*member) + 3; 
                                         member++;
                                         (*member) = (*member) + 3; 
                                         return (*member);
                                });
                printf("Lambda L-Value %d \n", bar());
        }

};

int main()
{
        int n = 0;
        int data[3] = { 0, 10, 100 };
        int * nptr = data;

        foo f1 = foo(nptr);
        f1();

        std::cout << " d : " << data[0] << std::endl;
        std::cout << " nptr : " << std::hex << nptr << std::endl;

        foo f2 = foo(nptr);
        f2();

        std::cout << " d : " << data[0] << std::endl;
        std::cout << " nptr : " << std::hex << nptr << std::endl;

        return 0;
}
