#include <iostream>

#define FOR_LOOP( VAR, TYPE, EXPR, COND, ITER, STMT ) \
        for_loop_tmpl( [=] (TYPE VAR) -> int  { return EXPR; }, \
                       [=] (TYPE VAR) -> bool { return COND; }, \
                       [=] (TYPE VAR) -> int  { ITER; return i;}, \
                       [&] (TYPE VAR) STMT )

class foo
{
        private:
                int member;

        public:

                foo(int val): member(val) { };

        void operator()()
        {
                auto bar  = ([=]  { return member + 3; });
                printf("Value %d \n", bar());
        }

};

int main()
{
        int n = 3;
        int data[3] = { 3, 4, 5};

        foo(0)();

        foo(2)();

        return 0;
}
