#include <iostream>

template<typename Finit, typename Fexit, typename Fiter, typename Fstmt>
void for_loop_tmpl(Finit init, Fexit exit, Fiter iter, Fstmt stmt)
{
        int i;
        for ( i = init(i) ; exit(i) ; i = iter(i) )
        {
                stmt(i);
                ;
        }
}


template<typename Finit, typename Fexit, typename Fiter, typename Fstmt>
void for_loop_tmpl_sycl(Finit init, Fexit exit, Fiter iter, Fstmt stmt)
{
        int i;
        for ( i = init(i) ; exit(i) ; i = iter(i) )
        {
                stmt(i);
                ;
        }
        
}


/*
for_loop_tmpl(
         [](int i) -> int  { return i = 0; },  // Init
         [](int i) -> bool { return i < 3; }, // Exit condition
         [](int i) -> int  { i++; return i;},   // Iterator
         [&](int i) { data[i] = i; }
        ); */

#define FOR_LOOP( VAR, TYPE, EXPR, COND, ITER, STMT ) \
        for_loop_tmpl( [=] (TYPE VAR) -> int  { return EXPR; }, \
                       [=] (TYPE VAR) -> bool { return COND; }, \
                       [=] (TYPE VAR) -> int  { ITER; return i;}, \
                       [&] (TYPE VAR) STMT )

int main()
{
        int n = 3;
        int data[3] = { 3, 4, 5};

        FOR_LOOP( i, int, (i = 0) , (i < n) , (i++), { data[i] = i; } );

        std::cout << " data " << data[0] << ", " << data[1] << ", " << data[2] << std::endl;

        return 0;
}
