#include <iostream>


namespace WrapCL
{       
        static int mock_mode;

        static void enable_mock_mode()
        {
                mock_mode = 1;
        }

        static void disable_mock_mode()
        {
                mock_mode = 0;
        }

        static bool mock_enabled() 
        {
                return (mock_mode == 1);
        }

        static int fcalled(std::string fname)
        {
                std::cout << "Calling " << fname << std::endl;
                if (mock_enabled())
                {
                        return 0;
                }
                else
                {
                        return 1;
                }
        }


        void check_err(int err)
        {
                std::cout << " Err " << err << std::endl;
        }

};



#define CL_CALL(fname, params) { if (WrapCL::fcalled("fname")) { int err = fname params; WrapCL::check_err(err); } }


int clFoo ( int * a, int * b );


int main()
{
        int n = 3;
        int data[3] = { 3, 4, 5};
        int out[3];

        CL_CALL ( clFoo, ( data, out ) );

        std::cout << out[0] << "," << out[1] << "," << out[2] << std::endl;

        return 0;
}



int clFoo ( int * a, int * b )
{
        b[0] = a[0];
        b[1] = a[1];
        b[2] = a[2];
        return 0;
}
