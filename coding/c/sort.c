#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#ifndef INT_MAX
#define INT_MAX ((unsigned int) -1)
#endif

#include "common.h"

const short SHOW = 10;

/* This should be 2*log(right - left), but since right-left is always < 2^32 
 *     we can set a maximum.
 */
#define MAX_RECUR 2*32 

/* Shows usage */
void usage(char * program_name);
/* Checks that the vector is ordered */
int check_order(int * vect, int n);

/* Simple ordering function */
void mindless(int * vect, int size);
void rr_qsort(int * vect, int size);


int main(int argc, char * argv[])
{
   int * vect;
   int n, i, broke;
   int algor;
   int ret;

   if (argc != 3)
   {
       usage(argv[0]);
       exit(1);
   }

   n = atoi(argv[1]);
   algor = atoi(argv[2]);
   ret = 0;

   vect = (int *) malloc(n * sizeof(int));

   for (i = 0; i < n; i++)
   {
      vect[i] = rand() ;
      if (i < SHOW)
      {
         printf("[%d] %d \n", i, vect[i]);
      }

   }
   printf("-----\n");

  switch (algor)
  {
      case 1:
            /* Mindless sorting */
            mindless(vect, n);
            break;
      case 2:
            /* System quick sort */
            qsort(vect, n, sizeof(int), &cmp_int);        
            break;
      case 3:
            /* My own qsort */
            rr_qsort(vect, n);
            break;
      default:
            printf(" Incorrect algorithm selected \n");
            ret = 1;
  }

  if (!ret)
  {
          ret = 0;
          if (check_order(vect, n))
          {
                  printf(" The vector was *not* ordered \n");
                  ret = 1;
          }
          /* SHOW first elements of vect */
         for (i = 0; i < (SHOW<n?SHOW:n); i++)
         {
             printf("[%d] %d \n", i, vect[i]);
         }
         printf("----- \n");
  }

   free(vect);
   return ret;
}

/* check_order
 * ------------------
 *  Checks that the elements of the vector are correctly ordered
 */
int check_order(int * vect, int n)
{
  int i;
  for (i = 1; (i < n) ; i++)
   {
      if (vect[i] < vect[i-1])
      {
         return 1;
      }
   }
   return 0;
}

void usage(char * program_name)
{
   printf(" Incorrect number of parameters \n");
   printf(" Usage: %s <vector size> <algorithm> \n", program_name);
   return;
}

/* mindless
* -------------- 
* Simple ordering function, swap elements until the vector is ordered 
*  Complexity worst case N**2  (the smallest element in the last position)
*/
void mindless(int * vect, int size)
{
   int i, change;
   do 
   {
     i = 1;
     change = 0;
     for (; i < size; i++)
     {
       if (vect[i - 1] > vect[i])
       {
          swap_int(vect + i - 1, vect + i);
          change = 1;
       }
     }
   }  while (change);
   return;
}



/* rr_qsort
*  -------------
*  Manual implementation of qsort 
*/

void rr_qsort_subset(int * vect, const int left, const int right);
void rr_qsort_subset_norecur(int * vect, const int p_left, const int p_right);

void rr_qsort(int * vect, const int size)
{
   /* rr_qsort_subset(vect, 0, size - 1); */
   rr_qsort_subset_norecur(vect, 0, size - 1);
}

void rr_qsort_subset(int * vect, const int left, const int right)
{
   int pivot, store;
   int i;

   if (left < right)
   {    

           /* We will use the middle of the subset for pivoting */
           pivot = left + ((right - left)>>1);  
           
           /* Elements on the left of the pivot must be smaller than the pivot */
           swap_int(vect + pivot, vect + right);
           store = left;
           for (i = left ; i < right; i++)
           {
              if (vect[i] < vect[right])
              {
                 if (i != store)
                 {  
                    swap_int(vect + i, vect + store);
                 }
                 store++;
              }
           }
            
           swap_int(vect + store, vect + right);
           /* The pivot is now at store */
           pivot = store;

           /* Sort the two remaining subsets */
           rr_qsort_subset(vect, left, pivot - 1);
           rr_qsort_subset(vect, pivot + 1, right);
           
   }
}


void rr_qsort_subset_norecur(int * vect, const int p_left, const int p_right)
{
   int pivot, store; 
   int stack_l[MAX_RECUR];  
   int stack_r[MAX_RECUR];
   int stack_top;
   int i, left, right;

   stack_l[0] = p_left;
   stack_r[0] = p_right;
   stack_top = 0;

   while (stack_top >= 0)
   {
      left = stack_l[stack_top];
      right = stack_r[stack_top];
      stack_top--;

      if (left < right)
      {    
           /* We will use the middle of the subset for pivoting */
           pivot = left + ((right - left)>>1);  
           
           /* Elements on the left of the pivot must be smaller than the pivot */
           swap_int(vect + pivot, vect + right);
           store = left;
           for (i = left ; i < right; i++)
           {
              if (vect[i] < vect[right])
              {
                 if (i != store)
                 {  
                    swap_int(vect + i, vect + store);
                 }
                 store++;
              }
           }
            
           swap_int(vect + store, vect + right);
           /* The pivot is now at store */
           pivot = store;

           /* Sort the two remaining subsets */
           /* rr_qsort_subset(vect, left, pivot - 1);
           rr_qsort_subset(vect, pivot + 1, right); */
           assert(++stack_top <= MAX_RECUR);

           stack_l[stack_top] = left;
           stack_r[stack_top] = pivot - 1;
           
           assert(++stack_top <= MAX_RECUR);

           stack_l[stack_top] = pivot + 1;
           stack_r[stack_top] = right;
           
      }
    }
}




