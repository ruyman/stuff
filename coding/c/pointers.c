#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char * argv[])
{
   char * p1    = strdup("01234567"); 
   char * p2    = p1;
   int   array[][3] = { { 1 , 2, 3 } ,
                         { 2, 1, 2 } };
   int  (*pa)[3] = array;

   printf(" p %s \n", p1);
   *p2 = 'K';
   printf(" p %s \n", p1);
   p2++;
   *p2 = 'K';
   printf(" p %s , p2 %s \n", p1, p2);

   *p2++ = 'K';
   printf(" p %s , p2 %s \n", p1, p2);

   *++p2 = 'K';
   printf(" p %s , p2 %s \n", p1, p2);

   printf("pa %d == 1 , pa + 1 %d == 2 \n", (*pa)[0], (*(pa + 1))[0]);

   1[1[array]] = 66;
 
   printf("Array [1][0] %d == 2 \n", array[1][0]);
   printf("Array [1][1] %d == 2 \n", array[1][1]);
   printf("Array [1][2] %d == 2 \n", array[1][2]);

   printf("Array [0][0] %d == 1 \n", array[0][0]);
   printf("Array [0][1] %d == 2 \n", array[0][1]);
   printf("Array [0][2] %d == 3 \n", array[0][2]);


   /* Uncomenting this one will make the free(p1) fail
     Because p1 is not NULL, but the memory has been freed !
    free(p2); */
   if (p1)
      free(p1);

   return 0;
}
