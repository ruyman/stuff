#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct _node
{
   int value;
   struct _node * next;
} TNode;


TNode * create_node(int val)
{
   TNode * tmp = (TNode *) malloc(sizeof(TNode));
   tmp->value = val;
   tmp->next = NULL;
   return tmp;
}

void add_node(TNode ** list, TNode * new)
{
   TNode * tmp = *list;
   if (list == NULL)
   {
      *list = new;
   }
   else
   {
      while (tmp->next != NULL)
      {
         tmp = tmp->next;
      }
      tmp->next = new;
   }
}

void del_node(TNode ** list, TNode * node)
{
   TNode * tmp = *list;
   /* If there is a node to delete, the list cannot be empty! */
   assert(list != NULL);
 
   if (!node)
   {
      return;
   }

   if (tmp == node)
   {
      printf("---\n");
      *list = tmp->next;
   } 
   else
   {
           while (tmp->next != node && tmp->next != NULL)
           {
                   tmp = tmp->next;
           }
           if (tmp->next != node)
           {
                   printf("Node is not in list \n");
                   return;
           }

      tmp->next = node->next;

   }

   free(node);

   return;
  
}

TNode * search_node(TNode * list, int val)
{
   TNode * tmp = list;
   
   while (tmp != NULL)
   {
      if (tmp->value == val)
      {
         break;
      }
      tmp = tmp->next;
   }  
   return tmp;
}

int cycle(TNode * list)
{
   TNode * p1, * p2;

   if (!list)
      return 0;

   p1 = list;

   if (!list->next)
   {
      return 0;
   }

   if (!list->next->next) 
   {
      return 0;
   }
   p2 = p1->next->next;

   while (p2 != p1 && p1 && p2)
   {
      if (!p1->next)
      { 
         return 0;
      }
      p1 = p1->next;
      if (!p2->next)
      {
         return 0;
      }
      if (!p2->next->next)
      {
         return 0;
      }
      p2 = p2->next->next;
   }
   
   if (p2 == p1)
   {
      return 1;
   }

   return 0;

}

int main(int argc, char * argv[])
{
   TNode * tmp, * list;

   tmp = create_node(4);
   list = tmp;

   tmp = create_node(6);

   add_node(&list, tmp);

   add_node(&list, create_node(-1));

   tmp = search_node(list, 4);

   del_node(&list, tmp);

   if (cycle(list))
   {
      printf(" There is a cycle \n");
   }
   else
   {
      printf(" There is no cycle \n");
   }

   tmp = list;
   if (tmp != NULL)
   {
     for (tmp = list; tmp != NULL; tmp = tmp->next)
     {
        printf("%d ->", tmp->value);       
     }
      printf("\n");
   }

   tmp = list;
   while (tmp)
   {
      list = tmp;
      tmp = tmp->next;
      free(list);
   }

   return 0;
}


