#include "common.h"

void swap_int(int * a, int * b)
{
   int tmp = *a;
   *a = *b; 
   *b = tmp;
}


int cmp_int(const void * a, const void * b)
{
   return *((int *) a) - *((int *) b);
}

