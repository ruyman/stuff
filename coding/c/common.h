/* Common definitions used in the test codes
*/


/* Swap to integer elements */
void swap_int(int * a, int * b);
/* Compare integers */
int cmp_int(const void * a, const void * b);
