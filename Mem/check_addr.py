#!/usr/bin/python
# check_addr.py
# ------------------------
#      Simpe program that reads a text file looking for memory address 
#       (hexadecimal numbers) and try to detect overlaps.
#      It was designed to detect memory errors. 
#      I created malloc and free hooks which printed the address and the size of
#      the pointer to stdout, and then I used this program to detect overlaps.
#      It receives a base address and a size. All memory addresses indentified in
#      the output are checked against this one.
#
import re
import sys


from area import Area

if len(sys.argv) < 3 or len(sys.argv) > 4:
	print " Incorrect argument number "
	sys.exit(1)

# Hexadecimal number
hex_re = re.compile("(0x[0-9a-fA-F]+)")
# Hexadecimal number followed by its size
pair_re = re.compile("(0x[0-9a-fA-F]+) size (\d+)")
# Lines to catch FREE operations
free_re = re.compile("FREE of .* (0x[0-9a-fA-F]+) ")

lines = None

if len(sys.argv) == 4:
   lines = open(sys.argv[4], 'r').readlines()
else:
   lines = open('test', 'r').readlines()

base = int(sys.argv[1],0)
size = int(sys.argv[2])

# Create a memory area instance with the information apssed as parameter
cginfo = Area(low = base, high = base + size)

prev_free = set()

for l in lines:
   # Ignore lines that do not contain hexadecimal numbers
	if l.find('0x') < 0: 
		continue
	for addr in hex_re.finditer(l):
		if cginfo.is_inside(int(addr.group(0),0)):
			print " Single address %s inside cginfo "%(addr.group(0))
	for free in free_re.finditer(l):
		addr = int(free.group(1),0)
		if cginfo.is_inside(addr):
			print " FREE of ptr %s "%(free.group(1))
		prev_free.add(addr)

	for pair in pair_re.finditer(l):
		low = int(pair.group(1),0)
		high = low + int(pair.group(2))
		tmp = Area(low = low, high = high)
		if cginfo.collides(tmp):
			print " Collision bw cginfo %s and tmp %s "%(cginfo, tmp)
			print " Line: " + str(l)

		if cginfo.after(tmp) and not low in prev_free:
			print " CGinfo %s is just after tmp %s "%(cginfo, tmp,)
			print " Line " + str(l)
	 	elif cginfo.after(tmp) and low in prev_free:
			print " After tmp %s but freed "%( tmp,)

