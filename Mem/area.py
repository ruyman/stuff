#!/usr/bin/python
#
#  Area

class Area(object):  
   """ Memory area class. 
       Contains properties for area boundaries and methods to check
       if two areas overlap
   """
           
   def __init__(self, low, high):
      self._low = low;
      self._high = high
      self._distance = 12 * 8

   def __str__(self,):
      return "%s ... %s "%(hex(self.low),hex(self.high))

   @property
   def low(self):
      return self._low
   @property
   def high(self):
      return self._high
   @property
   def distance(self):
      return self._distance

   def is_inside(self, pos):
      if pos >= self.low and pos <= self.high:
         return True
      return False
   
   def after(self, area):
      if area.high < self.low and ( (self.low - area.high) < self.distance): 
         return True
      return False

   def before(self, area):
      if area.low > self.high and ( (self.high - area.low) < self.distance):
         return True
      return False

   def collides(self, area):
      """ Check if area collides with self (i.e some address overlap) """
      if self.low < area.low and self.high > area.low:
        return True
      # self is completely inside area
      if self.low >= area.low and self.low <= area.high:
        return True
      # area is completely inside self
      if area.low >= self.low and area.low <= self.high:
        return True
      # self.low + size partialy overlaps area
      if self.high >= area.high and self.low <= area.low:
        return True
      return False
   


