#!/bin/bash 

install_dir=/Users/rreyesc/Projects/Stuff/Packages/tmp/

echo " Will create a soft directory with stuff in $install_dir "

function add_load() {
   local name=$1

   cd $SOFT/$name
   (
   echo " BASE=\$SOFT/$name "
   echo " export PATH=\$BASE/bin:\$PATH "
   echo " export LD_LIBRARY_PATH=\$BASE/lib64:\$BASE/lib:\$LD_LIBRARY_PATH "
   echo " export MANPATH=\$BASE/man:\$MANPATH " 
   echo " export ${name}_BASE=\$BASE "
   ) > load.sh
   cd $SOFTTMP
}

# Based in Stackoverflow: http://stackoverflow.com/questions/5379122/how-to-find-compress-method-of-archive-in-bash
# Support limited to tar for now
function extract()
{
     local fixed_path="--strip-components 1 "
     if [ -f $1 ] ; then
         case $1 in
             *.tar.bz2)   comm=$(tar $fixed_path  -xjf  $1 -C $2) ;;
             *.tar.gz)    comm=$(tar $fixed_path  -xzf  $1 -C $2)   ;;
#             *.bz2)       bunzip2 $1      ;;
#             *.rar)       unrar x $1      ;;
#             *.gz)        gunzip $1       ;;
             *.tar)       comm=$(tar $fixed_path  -xf  $1 -C $2)   ;;
             *.tbz2)      comm=$(tar $fixed_path  -xjf $1 -C $2)     ;;
             *.tgz)       comm=$(tar $fixed_path  -xzf $1  -C $2)     ;;
#             *.zip)       unzip $1        ;;
#             *.Z)         uncompress $1   ;;
#             *.7z)        7z x $1         ;;
             *)           echo "'$1' cannot be extracted via >extract<" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}

function install_package() {
   local name=$1
   local addr=$2
   local additional_config_opt=$3
   local preflags=$4
   # We assume the last part of the url is the filename  
   full_name=$(basename $addr)
   base_name=${fname%%.*}
   echo " Getting package $name "
   wget $addr &> /dev/null
   mkdir $name
   extract $full_name $name
   cd $name
   # configure + prefix
   echo " Configuring package $name "
   config=$($preflags ./configure --prefix=$SOFT/$name)
   # make
   echo " Building package $name "
   make=$(make)
   # make install
   echo " Installing package $name "
   make_install=$(make install)
   add_load $name 
   echo " Finished "
}


function abort ( ) {
   echo " !!! $1 "
   exit
}

oldpwd=$(pwd)

SOFT=$install_dir/soft
SOFTTMP=$SOFT/tmp

if [ -d $install_dir ]; then
   cd $install_dir
else
   abort " Cannot cd to install dir "
fi

mkdir soft
mkdir $SOFTTMP  
cd $SOFTTMP



install_package "m4" "http://ftp.gnu.org/gnu/m4/m4-latest.tar.bz2" ""
install_package "autoconf" "http://ftp.gnu.org/gnu/autoconf/autoconf-latest.tar.gz" ""
install_package "libtool" "http://ftp.gnu.org/gnu/libtool/libtool-2.4.tar.gz" ""

cd $oldpwd
